#!groovy

def before_mr_begin(pipeline_obj) {
    // MR target branch不满足要求则直接放过
    env.SKIP_AND_SUCCESS = "false"
    env.SKIP_AND_NOTHING = "false"
    if (env.gitlabTargetBranch && pipeline_obj['mrTargetBranch']?.trim()) {
        echo "The target branch is ${env.gitlabTargetBranch}"
        env.SKIP_AND_SUCCESS = "true"
        def regexKeyList = pipeline_obj['mrTargetBranch'].split(',')
        for (regex in regexKeyList) {
            echo "The regex pattern is ${regex}"
            def matcher = env.gitlabTargetBranch =~ ~/${regex}/
            if (matcher.matches()) {
                env.SKIP_AND_SUCCESS = "false"
            }
        }
    }
    if (env.SKIP_AND_SUCCESS == "true") {
        echo "Your branch will not trigger this pipeline, will skip RemainingStages!"
        return
    }

    env.TARGET_REPO_NAMESPACE = env.gitlabTargetRepoSshUrl.split(":")[1].split(".git")[0]
    if (pipeline_obj['repoXmlSshUrl']?.trim()) {
        echo "repo xml ssh url: ${pipeline_obj['repoXmlSshUrl']}"
        env.REPO_XML_NAMESPACE = pipeline_obj['repoXmlSshUrl'].split(":")[1].split(".git")[0]
        env.REPO_XML_SSH_URL = pipeline_obj['repoXmlSshUrl']
        env.REPO_XML_FILENAME = pipeline_obj['repoXmlFilename']
    } else {
        env.REPO_XML_NAMESPACE = ""
        env.REPO_XML_SSH_URL = ""
        env.REPO_XML_FILENAME = ""
    }
    // 1. 输出必要信息
    echo "repository url: ${env.gitlabSourceRepoSshUrl}"
    echo "source branch: ${env.gitlabSourceBranch}"
    echo "target branch: ${env.gitlabTargetBranch}"
    echo "repo namespace: ${env.REPO_XML_NAMESPACE}"

    // 获取当前提交仓库的url和token
    def (gitlabUrl, gitlabToken) = get_gitlab_url_and_token(env.gitlabSourceRepoHomepage)

    // 设置jenkins description
    handle_jenkins_pipeline_description()

    // 创建AutoMerge
    create_auto_merge_mr(gitlabUrl, gitlabToken)

    // merged跳过pipeline
    if (env.gitlabMergeRequestState.matches('merged')) {
        env.SKIP_AND_NOTHING = "true"
        currentBuild.result = 'ABORTED'
        error("Merged MR do not run build check")
    }

    // 已经通过的MR，没有代码变化不再跑pipeline
    if (check_pipeline_already_success(env.PIPELINE_NAME, gitlabUrl, gitlabToken)) {
        env.SKIP_AND_NOTHING = "true"
        currentBuild.result = 'ABORTED'
        error("The pipeline already success, will do nothing")
    }

    // 只有是repo的情况才跑
    if (env.REPO_XML_NAMESPACE?.trim()) {
        // 获取repo xml仓库的url和token
        def (xmlGitlabUrl, xmlGitlabToken) = get_gitlab_url_and_token(pipeline_obj['repoXmlSshUrl'])
        // 检查repo仓库是否有目标分支
        if (!check_repo_branch(xmlGitlabUrl, xmlGitlabToken, env.REPO_XML_NAMESPACE, env.gitlabTargetBranch)) {
            error("The ${env.REPO_XML_NAMESPACE} does not have branch ${env.gitlabTargetBranch}, can not do code check!")
        }
    }

    // 检查重复触发的pipeline
    handle_mr_duplicate()

    // 设置状态
    tools.update_gitlab_status(env.PIPELINE_NAME, 'running')
//    updateGitlabCommitStatus name: env.PIPELINE_NAME, state: 'running'

    // 检测冲突
    if (check_mr_conflict(gitlabUrl, gitlabToken)) {
        error("The mr has conflict, can not be merged.")
    }
}

def create_auto_merge_mr(String gitlabUrl, String gitlabToken) {
    if(!env.gitlabMergeRequestState.matches('merged')){
        echo "Open mr will not auto merge"
        return false
    }
    if(!env.gitlabTargetBranch.matches("release/(.*)")) {
        echo "MR is not release branch, will not auto merge"
        return false
    }
    env.MR_URL = env.gitlabSourceRepoHomepage + "/-/merge_requests/" + env.gitlabMergeRequestIid
    env.CREATOR_DETAILS = "THIS%20MERGE%20REQUEST%20IS%20CREATED%20BY%20MERGING%20THE%20MR_URL:%20${env.MR_URL}%20::%20Created%20By:%20${env.gitlabUserEmail}"
    def mr_id = create_auto_merge_mr_impl(gitlabUrl, gitlabToken)
    do_remove_approve(gitlabUrl, gitlabToken, mr_id)
}

def do_remove_approve(String gitlabUrl, String gitlabToken, String mrId) {
    sh "python3 ${env.STANDARD_CODE_PATH}/standard/scripts/gitlab_api.py --type \"rm_approval\" --url \"$gitlabUrl\" --token \"$gitlabToken\" --project_namespace \"${env.TARGET_REPO_NAMESPACE}\" --mr_id \"${mrId}\""
}

def get_build_jenkins_log_key_value(String console_log, String line_key, String line_split, Integer index) {
    try {
        console_log = console_log.replaceAll('display/redirect', '')
        def regex = /\/(\d+)\/consoleText/
        def matcher = console_log =~ regex
        def job_number = 0
        if (matcher.find()) {
            job_number = matcher.group(1)
        } else {
            echo "The ${console_log} log url is error."
            return ""
        }
        echo "The job number is ${job_number}"
        def logContent = Jenkins.getInstance().getItemByFullName(env.JOB_NAME).getBuildByNumber(Integer.parseInt(job_number)).logFile.text.toString()
        def lineList = logContent.split("\n")
        def key_value = ""
        for (line in lineList) {
            if (line.contains(line_key)) {
                echo "The line is ${line}"
                key_value = line.trim().split("${line_split}")[index]
                break
            }
        }
        echo "The key value is ${key_value}"
        return key_value
    } catch(Exception e) {
        echo "Error get key ${console_log},${line_key},${line_split},${index} from build log: ${e}"
        return ""
    }
}


def check_pipeline_already_success(String name='', String gitlabUrl='', String gitlabToken='') {
    // get commit pipeline status
    if(env.gitlabMergeRequestState.matches('merged')) {
        return false
    }
    if (!name?.trim()) {
        name = "default"
    }
    try {
        sh("curl -o last_commit.json --location --request GET '${gitlabUrl}/api/v4/projects/${env.gitlabMergeRequestTargetProjectId}/repository/commits/${env.gitlabMergeRequestLastCommit}/statuses' --header 'PRIVATE-TOKEN: ${gitlabToken}'")
        def last_commit_json = readJSON file: "last_commit.json", text: ''
        echo "Last commit pipeline status is ${last_commit_json}"
        last_commit_json = last_commit_json.findAll{ it["name"] == name }
        if (last_commit_json.size() == 0) {
            echo "The last commit has no pipeline status, will return"
            return false
        }
        def latest_index = last_commit_json.size() - 1
        if (!last_commit_json[latest_index]['target_url']?.trim() || !last_commit_json[latest_index]['target_url']) {
            echo "The last commit target url is null, will return"
            return false
        }
        def last_mr_log = last_commit_json[latest_index]['target_url'] + "consoleText"
        def last_mr_target_branch = get_build_jenkins_log_key_value(last_mr_log, "gitlabTargetBranch=", "=", 1)
        def last_mr_source_branch = last_commit_json[latest_index]['ref']
        def last_commit_status = last_commit_json[latest_index]['status']
        echo "Last mr target branch is ${last_mr_target_branch}, status is ${last_commit_status}, source branch is ${last_mr_source_branch}"
        if (last_commit_status == 'success' && last_mr_target_branch == env.gitlabTargetBranch && last_mr_source_branch == env.gitlabSourceBranch) {
            return true
        } else {
            return false
        }
    } catch(Exception e) {
        echo "Error on check pipeline status, error: ${e}"
        return false
    }
}

def check_repo_branch(String gitlab_url, String gitlab_token, String repo_name, String branch_name) {
    def encoded_repo_name = repo_name.replaceAll("\\/+","%2F")
    def encoded_branch_name = branch_name.replaceAll("\\/+","%2F")
    echo "encoded_repo: " + encoded_repo_name
    echo "encoded_branch: " + encoded_branch_name
    sh("curl -o branch_check.json --location --request GET '${gitlab_url}/api/v4/projects/${encoded_repo_name}/repository/branches/${encoded_branch_name}' --header 'PRIVATE-TOKEN: ${gitlab_token}'")
    def branch_check_json = readJSON file: "branch_check.json", text: ''
    def branch_check_info = branch_check_json['name']
    if (branch_check_info?.trim()) {
        echo "branch_check_info "+branch_check_info.toString()
        return true
    } else {
        return false
    }
}

def handle_mr_duplicate() {
    if(!env.gitlabMergeRequestState.matches('merged')) {
        //Removing duplicate MR Pipelines
        def latest_num = Jenkins.instance.getItemByFullName(env.JOB_NAME).getLastBuild().getNumber().toInteger()
        def building_list = []
        if (latest_num > 50) {
            for(i=latest_num-50; i<=latest_num; i++) {
                def build = Jenkins.instance.getItemByFullName(env.JOB_NAME).getBuildByNumber(i)
                if (build && build.isBuilding() && env.BUILD_NUMBER.toInteger() != build.getNumber().toInteger()) {
                    building_list.add(build)
                }
            }
        } else {
            building_list = Jenkins.instance.getItemByFullName(job_name).builds.findAll { it.isBuilding() && env.BUILD_NUMBER.toInteger() != it.getNumber().toInteger() }
        }
        echo "Building job list is ${building_list}"
        building_list.each { build ->
            echo "The build is ${build}"
            def logContent = build.logFile.text.toString()
            def lineList = logContent.split("\n")
            def building_mr_id = ""
            def building_mr_commitid = ""
            def building_mr_tar_branch = ""
            for (line in lineList) {
                if (line.contains("gitlabMergeRequestIid=")) {
                    echo "The line is ${line}"
                    building_mr_id = line.trim().split("=")[1]
                } else if (line.contains("gitlabMergeRequestLastCommit=")) {
                    echo "The line is ${line}"
                    building_mr_commitid = line.trim().split("=")[1]
                } else if (line.contains("gitlabTargetBranch=")) {
                    echo "The line is ${line}"
                    building_mr_tar_branch = line.trim().split("=")[1]
                }
                if (building_mr_id && building_mr_commitid && building_mr_tar_branch) {
                    echo "Find the results, ${building_mr_id}, ${building_mr_commitid}, ${building_mr_tar_branch}"
                    break
                }
            }
            if (building_mr_id == env.gitlabMergeRequestIid) {
                echo "The ${build} mr id is same with current mr id, will check abort!"
                if (building_mr_commitid == env.gitlabMergeRequestLastCommit && building_mr_tar_branch == env.gitlabTargetBranch) {
                    echo "The ${build} mr parameters is same with current mr. Will abort current mr!"
                    def cur_build = Jenkins.instance.getItemByFullName(env.JOB_NAME).getBuildByNumber(env.BUILD_NUMBER.toInteger())
                    cur_build.doStop()
                    cur_build.doKill()
                } else {
                    echo "The ${build} mr parameters is not same with current mr. Will abort the older mrs!"
                    build.doStop()
                    build.doKill()
                }
            }
        }
    }
}

def check_mr_conflict(String gitlabUrl, String gitlabToken) {
    sh("curl -o mr.json -k --location --request GET '${gitlabUrl}/api/v4/projects/${env.gitlabMergeRequestTargetProjectId}/merge_requests/${env.gitlabMergeRequestIid}' --header 'PRIVATE-TOKEN: ${gitlabToken}'")
    def mr_json = readJSON file: "mr.json", text: ''
    def has_conflicts = mr_json['has_conflicts'].toString()
    if (has_conflicts == 'false') {
        return false
    } else {
        return true
    }
}

def get_gitlab_url_and_token(String repositoryUrl) {
    if(repositoryUrl.contains("gitlab.com")) { // 美国区git仓库
        echo "env.GITLAB_US_TOKEN=${env.GITLAB_US_TOKEN}"
        return [env.GITLAB_US_URL, credentials('gitlab.com')]
    } else {
        error("The gitlab home url is error, ${repositoryUrl}, pls check!")
    }
}

def handle_jenkins_pipeline_description() {
//    currentBuild.description = "${env.gitlabSourceNamespace}/${env.gitlabSourceRepoName}/${env.gitlabSourceBranch}=>${env.gitlabTargetBranch} MR #${env.gitlabMergeRequestIid} ${env.gitlabMergeRequestState}"
    currentBuild.description = "Gitlab MR: <a href=\"${env.gitlabSourceRepoHomepage}/merge_requests/${env.gitlabMergeRequestIid}\">[${env.gitlabSourceNamespace}/${env.gitlabSourceRepoName}]${env.gitlabSourceBranch}=>${env.gitlabTargetBranch}#${env.gitlabMergeRequestIid}</a><br>MR State: ${env.gitlabMergeRequestState}"
}

