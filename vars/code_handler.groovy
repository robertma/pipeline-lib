#!groovy

def get_mirror_parameters(String repo_xml_namespace, String xml_filename="", String repo_mirror_base_folder="") {
    def repo_name = repo_xml_namespace.replaceAll("/", "_")
    def ecu_name = xml_filename.split("\\.")[0].replaceAll("/", "_")
    def repo_mirror_path = "${repo_mirror_base_folder}/${repo_name}/${ecu_name}"
    // 如果k8s yml中没有挂载nfs/standard_mirror_repo这个目录，即/data/mirror_repo这个目录不存在时
    def is_mirror_base_folder = sh(script: "test -d ${repo_mirror_base_folder} && echo '1' || echo '0' ", returnStdout: true).trim()
    if(is_mirror_base_folder=='0'){
        echo "The folder ${repo_mirror_base_folder} is not exist, will repo init without mirror!"
        return ""
    }

    // if (fileExists("${repo_mirror_base_folder}/${repo_name}/${ecu_name}")) {
    def res = sh(script: "test -d ${repo_mirror_path} && echo '1' || echo '0' ", returnStdout: true).trim()
    if(res=='1'){
        echo "The folder ${repo_mirror_path} is exist, will do mirror sync"
    } else {
        echo "The folder is not exist, will init mirror first, and then do mirror sync"
        // 在容器的/data/mirror-repo目录下操作，与nfs（132机器）的/data/nfs/standard_mirror_repo对应
        echo "============== Repo mirror init started ================"
        echo("the mirror repo path is: ${repo_mirror_path}")
        sh("mkdir -p ${repo_mirror_path}")
        sh("cd ${repo_mirror_path}; repo init -u ${env.REPO_XML_SSH_URL} -b develop --no-clone-bundle -m ${env.REPO_XML_FILENAME} --mirror; repo sync --force-sync")
        echo "============== Finish repo mirror init! ================"
    }
    return "--reference=${repo_mirror_path} --dissociate"
}

def merge_source_to_target(String code_folder, String source, String target) {
    dir(code_folder) {
        if (env.REPO_XML_NAMESPACE?.trim()) {
            sh("""#!/bin/bash
                while read line
                do
                    cd \$line
                    if git branch -av | awk '{print \$1}' | grep -w ${source}; then
                        git branch && git fetch
                        git checkout ${source} && git checkout ${target}
                        git merge --no-ff -m "[NVOS-000] AutoMerge from ${source} to ${target}" ${source}
                        if [ \$? != 0 ]; then
                            echo "The merge has conflict will skip"
                            git clean -fd
                            git reset --hard
                        fi
                    fi
                    cd -
                done < .repo/project.list
            """)
        } else {
            sh("""#!/bin/bash
                git merge --no-ff -m "[NVOS-000] AutoMerge from ${source} to ${target}" origin/${env.gitlabSourceBranch}
                if [ \$? != 0 ]; then
                    echo "The single repository merge has conflict will break"
                    exit 1
                fi
            """)
        }
    }
}

def sync_source_code() {
    sh("mkdir -vp ${env.CODE_FOLDER_NAME}")
    def manifest_branch = ""
    if(env.WORKFLOW_TEMPLATE == "mr"){
        manifest_branch = env.gitlabTargetBranch
    }else if(env.WORKFLOW_TEMPLATE.trim() == "build"){
        manifest_branch = env.MANIFEST_BRANCH
    }
    if (env.REPO_XML_SSH_URL?.trim()) {
        def mirror_parameters = get_mirror_parameters(env.REPO_XML_NAMESPACE, env.REPO_XML_FILENAME, env.REPO_MIRROR_BASE_DIR)
        dir(env.CODE_FOLDER_NAME) {
            sh("repo init -u ${env.REPO_XML_SSH_URL} -b ${manifest_branch} --no-clone-bundle -m ${env.REPO_XML_FILENAME} ${mirror_parameters}")
            sh("repo sync --force-sync")
            get_repo_module_path()
        }
    } else if (env.gitlabTargetRepoSshUrl?.trim()) {
        sh("git clone ${env.gitlabTargetRepoSshUrl} -b ${env.gitlabTargetBranch} ${env.CODE_FOLDER_NAME}")
    } else if (env.GIT_SSH_URL?.trim()) {
        sh("git clone ${env.GIT_SSH_URL} -b ${manifest_branch} ${env.CODE_FOLDER_NAME}")
    }
}

// 获取当前repo对应的module_path，给静态检查使用
def get_repo_module_path(){
    if(!env.gitlabTargetRepoSshUrl){
        // 非mr触发的无需获取module_path
        return
    }
    def current_repo_name = "${env.gitlabTargetRepoSshUrl}"
    current_repo_name = current_repo_name.split(":")[1].replace(".git","")  // ds/vdf/vdf-mcu/vdc-m7
    env.CURRENT_REPO_MODULE_PATH = sh(script: "set -x;repo forall \"${current_repo_name}\" -c \'echo \$REPO_PATH\'", returnStdout: true).trim()
    echo "The current repo module path is ${env.CURRENT_REPO_MODULE_PATH}"
}

def repo_pinned(){
    if (!env.REPO_XML_SSH_URL?.trim()) {
        echo "No repo xml will do not upload pinned.xml"
        return
    }
    dir(env.CODE_FOLDER_NAME) {
        sh("repo manifest -r -o pinned.xml")
        // archiveArtifacts 'changes.txt'
        // 默认上传本次pinned.xml到latest文件夹
        sh("curl -u \"${ARTIFACTORY_USER_PASS}\" -T pinned.xml \"${ARTIFACTORY_DIR_LATEST}\"")
        sh("curl -u \"${ARTIFACTORY_USER_PASS}\" -T pinned.xml \"${ARTIFACTORY_DIR}\"")
    }
}