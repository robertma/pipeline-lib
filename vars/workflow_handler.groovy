#!groovy
import groovy.json.JsonOutput

def post_action(status) {
    // SKIP_AND_NOTHING已经设置过currentBuild状态为abort，直接return
    if (env.SKIP_AND_NOTHING == "true") {
        echo "env.SKIP_AND_NOTHING=true, Will do nothing"
        return
    }
    // SKIP_AND_SUCCESS不需要发送飞书，只需要更新状态即可
    if (env.SKIP_AND_SUCCESS == "true") {
        tools.update_gitlab_status(env.PIPELINE_NAME,"success")
        // updateGitlabCommitStatus name: env.PIPELINE_NAME, state: "success"
        return
    }

    if(env.DEBUG_MODE == "true" && status == "failed"){
        // 对于开启了debug模式的流水线，需要先更新MR状态并发送飞书，才能进入troubleshooting阶段
        tools.update_gitlab_status(env.PIPELINE_NAME,"failed")
        tools.send_feishu("failed")
        tools.do_debug()
        return
    }
    // 其他情况，则更新MR状态并发送飞书
    tools.update_gitlab_status(env.PIPELINE_NAME,"$status")
    tools.send_feishu(status)
}

// 串联yml中的stage，只允许parallel一层嵌套
def do_stages(pipeline_obj){
    def items = pipeline_obj['stages']
    for(item in items){
        def stageName = item["stageName"]
        def cmd = item['cmd']
        def parallel_stages = item['parallelStages']
        if(parallel_stages != null){
            // 并行stage处理
            do_stage_parallel(item)
            continue
        }else if(cmd != null){
            // 一般stage处理
            do_stage_cmd(stageName,cmd)
            continue
        }else{
            echo "no cmd is configed in stage: $stageName"
        }
    }
}

def do_stage_parallel(item){
    def stageName = item["stageName"]
    def parallel_stages = item['parallelStages']
    echo "parallel_stages: $parallel_stages"
    if(parallel_stages == null){
        echo "no parallel configed in stageName: $stageName"
        return
    }
    def jobs = [:]
    stage("$stageName") {
        // 并行stage处理
        for (x in parallel_stages) {
            def stageName_parallel = x["stageName"]
            def cmd_parallel = x["cmd"]
            def subStages = x["subStages"]
            if (subStages != null) {
                jobs[stageName_parallel] = {
                    stage("$stageName_parallel") {
                        for(sub in subStages){
                            do_stage_cmd(sub["stageName"],sub["cmd"])
                        }
                    }
                }
                continue
            }
            if (cmd_parallel != null) {
                jobs[stageName_parallel] = {
                    do_stage_cmd(stageName_parallel,cmd_parallel)
                }
            } else {
                echo "No cmd provided, skip this stage: ${stageName_parallel}"
            }
        }
        parallel jobs
    }
}

def do_stage_cmd(def stageName,def cmd){
//    def cmd_str = cmd.join("\n")
    stage("$stageName") {
        echo "===>  stage_name is: ${stageName}, start to exec cmd: ${cmd} <==="
        do_cmd(cmd)
    }
}

def do_cmd(def cmd){
    sh """#!/bin/bash
             set -e
             $cmd
             env | grep "CUSTOM_" | cat > ${WORKSPACE}/env.txt
        """
    post_do_cmd()
}

def post_do_cmd(){
    // 1. 将新增的环境变量加入到系统环境变量中，对已有环境变量做更新不生效
    def new_env = readFile(file: "${WORKSPACE}/env.txt")
    for(line in new_env.readLines()){
        // 对于用户自定义的环境变量export CUSTOM_XXX=""，自动加入到系统环境变量中
        if(line.contains("CUSTOM_")){
            def key = line.split("=")[0]
            def value = line.split("=")[1]
            if(!env.getProperty(key)){
                echo "Find new custom define environment, env_name=$key"
            }
            env.setProperty(key,value)
        }
    }
}

// 解析yml，返回pipeline_obj，并加载必要信息到环境变量
def parse_yml(yml_file) {
    def job_yml = readYaml file : yml_file
    sh """
        echo "此流水线对应的yml文件内容为：\n"
        cat ${yml_file} 
    """
    def pipeline_obj = job_yml['pipeline']
    // env.FEISHU_URL
    env.FEISHU_URL = pipeline_obj['feishuNotifyUrl']
    // env.WORKFLOW_TEMPLATE
    def workflowTemplate = pipeline_obj['workflowTemplate']
    if(workflowTemplate==null || workflowTemplate.trim()=="" ){
        env.WORKFLOW_TEMPLATE = "free"
    }else{
        env.WORKFLOW_TEMPLATE = workflowTemplate
    }
    return pipeline_obj
}

def config_k8s_pod(pipeline_obj){
//    env.STANDARD_SCRIPT_PATH = "standard_scripts/"
    // 获取用户定义的docker image
    def dockerImage = "artifactory-cn.nevint.com:5017/bgw/ubuntu_dind:latest"
    def k8sYamlPath_custom = pipeline_obj['k8sYamlPath']
    def dockerImage_custom = pipeline_obj['dockerImage']
    if(k8sYamlPath_custom != null && k8sYamlPath_custom.trim() !=""){
        k8sYamlPath_custom = "${k8sYamlPath_custom}"
        echo "custom has defined k8s yml file，k8sYamlPath_custom: $k8sYamlPath_custom"
        k8s_yml_text = sh(returnStdout: true, script: "cat ${k8sYamlPath_custom}")
    }else{
        if(dockerImage_custom != null && dockerImage_custom.trim()!="" ){
            echo "custom input dockerImage is : $dockerImage_custom"
            dockerImage = dockerImage_custom
        }else{
            echo "Yml did not define dockerImage and k8sYamlPath, will use default k8s yml and docker image!"
        }
        k8s_yml_text = """
        apiVersion: v1
        kind: Pod
        spec:
          hostAliases:
           - ip: "10.118.0.48"
             hostnames:
              - "nevaflex01"
          imagePullSecrets:    
            - name: docker-secret-5017
          securityContext:
            runAsUser: 0
          volumes:
            - name: www
              persistentVolumeClaim:
                claimName: pvc-nfs-132
          containers:
            - name: default
              image: "$dockerImage"
              securityContext:
                privileged: true
              imagePullPolicy: Always
              tty: true
              volumeMounts:
                - name: www
                  subPath: standard_mirror_repo
                  mountPath: /data/mirror_repo
        """
    }
    echo "will use k8s_yml_text: $k8s_yml_text"
    return k8s_yml_text
}

def do_pre_custom_check(pipeline_obj){
    // 在Code Prepare之前做一些前置检查，为用户自定义。如果脚本执行成功则继续往下走，如果执行失败则不会走接下来的步骤
    def preCodeCmd = pipeline_obj['preCodeCmd']
    if(preCodeCmd!=null){
        try{
            do_cmd(preCodeCmd)
        }catch (Exception e) {
            println "preCodeCmd exec Exception :" + e
            echo "PreCodeCmd exec failed, the pipeline will skip RemainingStages"
            env.SKIP_AND_SUCCESS = "true"
        }
    }
}

def before_build_begin(pipeline_obj){
    // env.TARGET_REPO_NAMESPACE = env.gitlabTargetRepoSshUrl.split(":")[1].split(".git")[0]
    if (pipeline_obj['repoXmlSshUrl']?.trim()) {
        echo "repo xml ssh url: ${pipeline_obj['repoXmlSshUrl']}"
        env.REPO_XML_NAMESPACE = pipeline_obj['repoXmlSshUrl'].split(":")[1].split(".git")[0]
        env.REPO_XML_SSH_URL = pipeline_obj['repoXmlSshUrl']
        env.REPO_XML_FILENAME = pipeline_obj['repoXmlFilename']
    } else if (pipeline_obj['gitSshUrl']?.trim()) {
        env.GIT_SSH_URL = pipeline_obj['gitSshUrl']
    } else {
        echo "For build template, no repo config, will failed!"
        sh "exit 1"
    }
    // 如果有MANIFEST_BRANCH参数
    if(env.MANIFEST_BRANCH && env.MANIFEST_BRANCH.trim()!=""){
        echo "MANIFEST_BRANCH =$env.MANIFEST_BRANCH"
        env.MANIFEST_BRANCH=env.MANIFEST_BRANCH.trim()
    }else{
        env.MANIFEST_BRANCH = ""
    }
    // env.ARTIFACTORY_DIR
    def artifactoryPath = pipeline_obj['artifactoryDir']
    def manifest_branch = env.MANIFEST_BRANCH.replace("/","_")
    if(artifactoryPath==null || artifactoryPath.trim()==""){
        env.ARTIFACTORY_DIR="${env.DEFAULT_ARTIFACTORY_BASE_DIR}${env.JOB_NAME}/${manifest_branch}/${BUILD_NUMBER}/"
        env.ARTIFACTORY_DIR_LATEST="${env.DEFAULT_ARTIFACTORY_BASE_DIR}${env.JOB_NAME}/${manifest_branch}/latest/"
    }else{
        if(artifactoryPath.trim().endsWith("/")){
            artifactoryPath = artifactoryPath.trim()
        }else{
            artifactoryPath = artifactoryPath.trim()+"/"
        }
        env.ARTIFACTORY_DIR="${artifactoryPath}${manifest_branch}/${BUILD_NUMBER}/"
        env.ARTIFACTORY_DIR_LATEST="${artifactoryPath}${manifest_branch}/latest/"
    }
    currentBuild.description = "Manifest Branch: ${env.MANIFEST_BRANCH}<br>Build Artifact: <a href=\"${env.ARTIFACTORY_DIR}\">${env.BUILD_NUMBER}</a>"
}

// Send build history to web
def send_build_history_to_web() {
    def pipeline_id = "$env.WEB_PIPELINE_ID"
    if(pipeline_id?.trim() && pipeline_id.isInteger()){
        echo "The web pipeline id is ${env.WEB_PIPELINE_ID}"
    }else{
        echo "This pipeline is not generated by web, will not send build info to web"
        return
    }
    def job_url = "${env.BUILD_URL}"
    def status = currentBuild.result
    def trigger = ''
    if (env.gitlabMergedByUser?.trim()) {
        trigger = env.gitlabMergedByUser
    } else if (env.BUILD_USER_ID?.trim()) {
        trigger = env.BUILD_USER_ID
    } else {
        trigger = 'Timer'
    }
    def artifactory_url = env.BUILD_URL
    def build_parameters = ["log": env.BUILD_URL, "img": env.BUILD_URL, 'job_url': env.JOB_URL]
    def description = "Build log ${env.BUILD_URL}"

    sh("ls -al ${env.WORKSPACE}")
    def body_map = [:]
    body_map["pipeline"] = pipeline_id.toInteger()
    body_map["build_url"] = job_url
    body_map["status"] = status
    body_map["trigger"] = trigger
    body_map["artifactory_url"] = artifactory_url
    body_map["description"] = description
    body_map["build_parameters"] = build_parameters
    def body_json_str = JsonOutput.toJson(body_map).replaceAll("\"", "\\\"").replaceAll(" ", "")
    withCredentials([usernamePassword(credentialsId: 'ctd-rocket-api', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
        sh("""
            python3 ${env.STANDARD_CODE_PATH}/standard/scripts/web_api.py --type cicd_post_build --url "https://rocket-pro.nioint.com" --username "${USERNAME}" --password "${PASSWORD}" --build_data '${body_json_str}'
        """)
    }
}