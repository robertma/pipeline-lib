#!groovy

// 更新gitlab状态时加上重试机制，每一次都默认重试3次，以防更新失败的情况
def update_gitlab_status(name,status){
    // 非mr直接跳过
    if(!env.gitlabTargetBranch){
        return
    }
    def count = 0
    while(true){
        if(count>2){
            break
        }
        echo "the $count time: update gitlab mr status: ${status}"
        updateGitlabCommitStatus(name: name, state: status)
        sh "sleep 1s"
        count = count + 1
    }
}

// 根据触发的仓库动态获取gitlabConnection
def get_gitlabconnection(){
    def git_connection = "gitlab"
    if(env.gitlabTargetRepoHttpUrl){
        def str = env.gitlabTargetRepoHttpUrl.replace("https://","").replace(".git","")
        //git_connection = str.split("/")[0]
        echo "The dynamic gitlabConnection is $git_connection"
    }
    return git_connection
}

// 发送飞书群消息（简单版本）
// 需提前设置env.FEISHU_URL，env.WORKFLOW_TEMPLATE，env.ARTIFACTORY_DIR，
// env.FEISHU_URL不可为空
def send_feishu(status){
    if (!env.FEISHU_URL?.trim() || !status?.trim()) {
        echo "Feishu url or status is null, will not send message"
        return
    }
    def build_time = new Date()
    if(env.WORKFLOW_TEMPLATE && env.WORKFLOW_TEMPLATE=="build" && status=="success"){
        def image_url = "${env.ARTIFACTORY_DIR}"
        sh("echo '''{ \"msg_type\": \"post\", \"content\": { \"post\": { \"zh_cn\": { \"title\": \"standard pipeline $status\", \"content\": [[{\"tag\": \"text\",  \"text\": \"job name: \n\"}, {\"tag\": \"a\", \"text\": \"\n${env.JOB_BASE_NAME}\n\", \"href\": \"${env.BUILD_URL}\" }], [{\"tag\": \"text\",  \"text\": \"\n\nbuild time: ${build_time}\n\"}],[{\"tag\": \"text\",  \"text\": \"\n\nbuild number: ${env.BUILD_NUMBER}\n\"}], [{\"tag\": \"text\",  \"text\": \"\nimage: ${image_url}\n\"}]]}}}}''' > req.json")
    }else{
        sh("echo '''{ \"msg_type\": \"post\", \"content\": { \"post\": { \"zh_cn\": { \"title\": \"standard pipeline $status\", \"content\": [[{\"tag\": \"text\",  \"text\": \"job name: \n\"}, {\"tag\": \"a\", \"text\": \"\n${env.JOB_BASE_NAME}\n\", \"href\": \"${env.BUILD_URL}\" }], [{\"tag\": \"text\",  \"text\": \"\n\nbuild time: ${build_time}\n\"}],[{\"tag\": \"text\",  \"text\": \"\n\nbuild number: ${env.BUILD_NUMBER}\n\"}]]}}}}''' > req.json")
    }
    sh("curl --fail -v -H '''Content-Type: application/json''' -X POST ${env.FEISHU_URL} -d @req.json")
}

// 容器化流水线中增加debug步骤
def do_debug(){
    stage("TroubleShooting"){
        echo "执行失败，默认预留2小时用于问题排查，2小时后容器会自动销毁！请通过跳板机登录容器排查问题，当前容器的IP地址如下所示："
        sh "set +e;sudo service ssh start;ip a;sleep 7200s"
    }
}
