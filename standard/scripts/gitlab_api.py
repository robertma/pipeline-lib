#!/usr/bin/python3

import gitlab
import sys
import base64
import requests
from retry import retry
import logging
import re
import argparse
import time
from xml_parse import XmlParse

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s - %(filename)s[line:%(lineno)d] - %(levelname)s: %(message)s')
from feishu_api import Send_Group_Message

# to disable warning info
from requests.packages.urllib3.exceptions import InsecureRequestWarning

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


# for the gitlab api exception
def on_exception(actual_do):
    def wrap(*args, **keyargs):
        try:
            return actual_do(*args, **keyargs)
        except Exception as e:
            logging.error("Error execute gitlab function: %s, error info: %s" % (actual_do.__name__, e))
            if actual_do.__name__ == 'auto_merge':
                send_feishugroup(args[0])
            # sys.exit(1)

    return wrap


def send_feishugroup(mr_id):
    logging.info("Auto merge failed send FeishuGroup Mr_id:%s" % (mr_id))
    # group is Just A Test
    group_token = "https://open.feishu.cn/open-apis/bot/v2/hook/e6c4c737-6f08-47f3-a0dd-2473a7cdc519"
    feishu_api = Send_Group_Message()
    card_json = {
        "config": {
            "wide_screen_mode": True
        },
        "elements": [{
            "extra": {
                "tag": "button",
                "text": {
                    "content": "jenkins_url",
                    "tag": "plain_text"
                },
                "type": "primary",
                "url": 'https://jenkins-ds.corp.nio.io/'
            },
            "tag": "div",
            "text": {
                "content": "◉MR_ID: %s\n" % (mr_id),
                "tag": "lark_md"
            }
        }],
        "header": {
            "template": "red",
            "title": {
                "content": "Rc Auto Merge Failure",
                "tag": "plain_text"
            }
        }
    }
    group_msg_json = {
        "msg_type": "interactive",
        "card": card_json
    }
    feishu_api.send_group_message(group_token, group_msg_json)


class Gitlab:
    """
        gitlab api request
    """

    def __init__(self, token, gitlab_url):
        if token and gitlab_url:
            self.ghost = gitlab.Gitlab(gitlab_url, private_token=token, api_version=4, ssl_verify=False)
        else:
            logging.error("Error: the gitlab host or token is unknow")
            sys.exit(1)

    @on_exception
    def get_project_by_name(self, namespace):
        return self.ghost.projects.get(namespace)

    @on_exception
    def get_branch_list_by_name(self, namespace):
        project = self.get_project_by_name(namespace)
        return project.branches.list(all=True)

    @on_exception
    def get_tag_list_by_name(self, namespace):
        project = self.get_project_by_name(namespace)
        return project.tags.list(all=True)

    @on_exception
    def get_file_obj_and_content(self, namespace, filepath, ref):
        project = self.get_project_by_name(namespace)
        file_obj = project.files.get(file_path=filepath, ref=ref)
        content = base64.b64decode(file_obj.content)
        return file_obj, content

    @on_exception
    def update_file(self, file_obj, content, ref, message):
        file_obj.content = content
        file_obj.save(branch=ref, commit_message=message)

    @on_exception
    def get_group_mr_list(self, group_name, updated_after=None):
        group_obj = self.ghost.groups.get(group_name)
        if updated_after:
            return group_obj.mergerequests.list(all=True, state='all', updated_after=updated_after)
        return group_obj.mergerequests.list(all=True, state='all')


class GitlabProject:
    """
        gitlat project api
    """

    @on_exception
    def __init__(self, namespace, token, gitlab_url="https://githost.nevint.com/"):
        gitlab = Gitlab(token, gitlab_url)
        self.project = gitlab.ghost.projects.get(namespace)
        self.namespace = namespace

    @on_exception
    def get_file_obj_and_content(self, filepath, ref):
        file_obj = self.project.files.get(file_path=filepath, ref=ref)
        content = bytes.decode(base64.b64decode(file_obj.content))
        return file_obj, content

    @on_exception
    def get_file_list(self, folder_path, ref):
        return self.project.repository_tree(path=folder_path, ref=ref)

    """
        this func will update file directly without mr
    """

    @on_exception
    @retry(exceptions=Exception, tries=3, delay=2, backoff=1)
    def update_file(self, file_obj, content, ref, message):
        file_obj.content = content
        file_obj.save(branch=ref, commit_message=message)

    @on_exception
    def get_branch_list_by_search(self, keyword):
        obj_list = self.project.branches.list(search=keyword, all=True)
        return [item.name for item in obj_list]

    @on_exception
    def get_tag_list_by_search(self, keyword):
        obj_list = self.project.tags.list(search=keyword, all=True)
        return [item.name for item in obj_list]

    """
        result['commits'] get the diff commits
        result['diffs] get the diff files
        ref1: the start branch
        ref2: the end branch
        from ref1 to ref2, there must be different commits, otherwise the output is empty list
    """

    @on_exception
    def compare_two_ref(self, ref1, ref2):
        logging.info("Start %s, end %s" % (ref1, ref2))
        return self.project.repository_compare(ref1, ref2)

    @on_exception
    def create_tag(self, tag_name, ref, message):
        o_list = [item.name for item in self.project.tags.list(search=tag_name)]
        if tag_name in o_list:
            logging.info("%s: Tag already exist with: %s" % (self.namespace, tag_name))
        else:
            s_branch_list = [item.name for item in self.project.branches.list(search=ref)]
            s_tag_list = [item.name for item in self.project.tags.list(search=ref)]
            if ref in s_branch_list or ref in s_tag_list:
                logging.info("%s: Will create tag %s from %s" % (self.namespace, tag_name, ref))
                try:
                    self.project.tags.create({'tag_name': tag_name, 'ref': ref, 'message': message})
                except Exception as e:
                    if "Protected" in str(e):
                        logging.info("The tag is protected, will remove the protected tag and create one")
                        pattern_tag = self.get_protected_tag_by_re(tag_name)
                        self.delete_protected_tag(pattern_tag)
                        self.project.tags.create({'tag_name': tag_name, 'ref': ref, 'message': message})
                        self.create_protected_tag(pattern_tag)
                    else:
                        logging.error("Create tag error : %s" % (e))
            else:
                logging.info("%s: Will skip create tag, because the ref %s is not exist" % (self.namespace, ref))

    @on_exception
    def delete_tag(self, tag_name):
        o_list = self.project.tags.list(search=tag_name)
        if not o_list:
            logging.info("%s: Tag not exist with: %s, no need to delete" % (self.namespace, tag_name))
        else:
            logging.info("%s: Will delete tag %s" % (self.namespace, tag_name))
            try:
                self.project.tags.delete(tag_name)
            except Exception as e:
                if "Protected" in str(e):
                    logging.info("The tag is protected, will remove the protected tag and delete one")
                    pattern_tag = self.get_protected_tag_by_re(tag_name)
                    self.delete_protected_tag(pattern_tag)
                    self.project.tags.delete(tag_name)
                    self.create_protected_tag(pattern_tag)
                else:
                    logging.error("Delete tag error : %s" % (e))

    @on_exception
    def create_branch(self, branch_name, ref):
        result = False
        o_list = [item.name for item in self.project.branches.list(search=branch_name)]
        if branch_name in o_list:
            logging.info("%s: Branch already exist with: %s" % (self.namespace, branch_name))
            result = True
        else:
            s_branch_list = [item.name for item in self.project.branches.list(search=ref)]
            s_tag_list = [item.name for item in self.project.tags.list(search=ref)]
            if ref in s_branch_list or ref in s_tag_list:
                logging.info("%s: Will create branch %s from %s" % (self.namespace, branch_name, ref))
                try:
                    self.create_branch_retry(branch_name, ref)
                except Exception as e:
                    logging.error("%s create branch error!" % (self.namespace))
                result = True
            else:
                logging.info("%s: Will skip create branch, because the ref %s is not exist" % (self.namespace, ref))
        return result

    @on_exception
    @retry(exceptions=Exception, tries=3, delay=2, backoff=1)
    def create_branch_retry(self, branch_name, ref):
        self.project.branches.create({'branch': branch_name, 'ref': ref})
        o_list = [item.name for item in self.project.branches.list(search=branch_name)]
        retry_num = 0
        while branch_name not in o_list:
            logging.info("%s: Branch created but not exist with: %s" % (self.namespace, branch_name))
            time.sleep(5)
            try:
                self.project.branches.create({'branch': branch_name, 'ref': ref})
            except Exception as e:
                logging.error("%s create error, %s" % (branch_name, e))
                if "Branch already exists" in str(e):
                    logging.info("Create ok will exist")
                    break
            o_list = [item.name for item in self.project.branches.list(search=branch_name)]
            retry_num += 1
            if retry_num > 10:
                logging.error("Retry more than 10 times. Will break")
                break

    @on_exception
    def delete_branch(self, branch_name):
        o_list = self.project.branches.list(search=branch_name)
        if not o_list:
            logging.info("%s: Branch not exist with: %s, no need to delete" % (self.namespace, branch_name))
        else:
            logging.info("%s: Will delete branch %s" % (self.namespace, branch_name))
            self.project.branches.delete(branch_name)

    # remove source branch when mr is merged
    @on_exception
    def create_merge_request(self, source_branch, target_branch, mr_title, remove_source_branch):
        mr_list = self.project.mergerequests.list(source_branch=source_branch,
                                                  target_branch=target_branch, state='opened')
        if mr_list:
            logging.info("The mr list is exist %s" % (mr_list))
            return mr_list[0]
        mr = self.project.mergerequests.create({'source_branch': source_branch,
                                                'target_branch': target_branch,
                                                'title': mr_title,
                                                'squash': False,
                                                'remove_source_branch': remove_source_branch})
        return mr

    def get_mr_by_id(self, mr_id):
        return self.project.mergerequests.get(mr_id)

    @on_exception
    def remove_mr_approve(self, mr_id):
        mr = self.get_mr_by_id(mr_id)
        project_mr_approvals = self.get_project_mr_approval()
        project_origin_disable_flag = project_mr_approvals.attributes.get(
            "disable_overriding_approvers_per_merge_request")
        if project_origin_disable_flag:
            self.update_project_mr_approval(disable_overriding_approvers_per_merge_request=False)
        mr_approvals = mr.approvals.get()
        if mr_approvals.approvals_required == 0:
            logging.info("The mr is already has no approvals, will no need to remove")
        else:
            logging.info("Need to remove approvals")
            self.set_mr_no_approval(mr)
        if project_origin_disable_flag:
            self.update_project_mr_approval(disable_overriding_approvers_per_merge_request=True)

    @on_exception
    @retry(exceptions=Exception, tries=5, delay=5, backoff=3)
    def auto_merge(self, mr_id):
        mr = self.get_mr_by_id(mr_id)
        logging.info("Auto Merge MR_ID: %s ,MR URL is: %s" % (mr_id, mr.web_url))
        while mr.merge_status == "checking":
            time.sleep(5)
            mr = self.get_mr_by_id(mr_id)
            logging.info("Sleep 5s and the mr status is %s" % (mr.merge_status))
        if mr.state == 'merged':
            print("The mr %s: %s has already been merged" % (self.namespace, mr_id))
            return True
        if mr.merge_status == "cannot_be_merged":
            logging.error("The mr status can not be merged")
            return False
        project_mr_approvals = self.get_project_mr_approval()
        project_origin_disable_flag = project_mr_approvals.attributes.get(
            "disable_overriding_approvers_per_merge_request")
        if project_origin_disable_flag:
            self.update_project_mr_approval(disable_overriding_approvers_per_merge_request=False)
        merge_flag = False
        mr_approvals = mr.approvals.get()
        if mr_approvals.approvals_required == 0:
            logging.info("The mr is already has no approvals, will merge directedly")
        else:
            logging.info("Need to be removed, and then merged")
            self.set_mr_no_approval(mr)
        retry_num = 0
        logging.info("Will set pipeline success")
        self.set_mr_pipeline_success(mr)
        while mr.head_pipeline is None or mr.head_pipeline["status"] != "success":
            self.set_mr_pipeline_success(mr)
            time.sleep(5)
            mr = self.get_mr_by_id(mr_id)
            if mr.head_pipeline is None:
                logging.info("The mr pipeline is None")
            else:
                logging.info("Sleep 5s and the pipeline status is %s" % (mr.head_pipeline["status"]))
            retry_num += 1
            if retry_num > 10:
                logging.error("Retry more than 10 times. Will break")
                break
        logging.info("Trying to merge")
        try:
            mr_result = mr.merge(merge_when_pipeline_succeeds=True, should_remove_source_branch=True)
        except Exception as e:
            logging.error("Merge with pipeline success error: %s" % e)
            mr_result = mr.merge(should_remove_source_branch=True)
            logging.info("Merge directly again")
        if project_origin_disable_flag:
            self.update_project_mr_approval(disable_overriding_approvers_per_merge_request=True)
        if "merge_error" in mr_result and mr_result['merge_error']:
            logging.info("Merge error result is %s" % mr_result['merge_error'])
        else:
            logging.info("Merge result is %s" % mr_result)
        if mr_result['state'] == "merged":
            logging.info("The mr has been successed merged!")
            merge_flag = True
            return merge_flag
        else:
            self.auto_merge_failed_send_feishugroup(mr_id)
            return merge_flag

    @on_exception
    def auto_merge_failed_send_feishugroup(self, mr_id):
        print("auto_merge_failed_send_feishu")
        mr = self.get_mr_by_id(mr_id)
        web_url = mr.web_url
        source_branch = mr.source_branch
        target_branch = mr.target_branch
        # group is Just A Test
        # group_token = "https://open.feishu.cn/open-apis/bot/v2/hook/e6c4c737-6f08-47f3-a0dd-2473a7cdc519"
        group_token = "https://open.feishu.cn/open-apis/bot/v2/hook/afd65386-9351-4971-b792-d8bd26cc68d8"
        feishu_api = Send_Group_Message()
        card_json = {
            "config": {
                "wide_screen_mode": True
            },
            "elements": [{
                "extra": {
                    "tag": "button",
                    "text": {
                        "content": "mr_url",
                        "tag": "plain_text"
                    },
                    "type": "primary",
                    "url": web_url
                },
                "tag": "div",
                "text": {
                    "content": "◉repo_namespace: %s\n◉source_branch: %s\n◉target_branch: %s" % (
                    self.namespace, source_branch, target_branch),
                    "tag": "lark_md"
                }
            }],
            "header": {
                "template": "red",
                "title": {
                    "content": "RC Auto Merge Failure",
                    "tag": "plain_text"
                }
            }
        }
        group_msg_json = {
            "msg_type": "interactive",
            "card": card_json
        }
        feishu_api.send_group_message(group_token, group_msg_json)

    """
        disable_overriding_approvers_per_merge_request: True
    """

    @on_exception
    def update_project_mr_approval(self, **kwargs):
        self.project.approvals.update(**kwargs)
        approvals = self.project.approvals.get()
        logging.info("The new approvals is %s" % (approvals))
        return approvals

    @on_exception
    def get_project_mr_approval(self):
        return self.project.approvals.get()

    @on_exception
    def set_mr_no_approval(self, mr):
        mr.approvals.set_approvers(approvals_required=0)

    @on_exception
    def set_mr_pipeline_success(self, mr):
        merge_commit_id = mr.sha
        commit = self.project.commits.get(merge_commit_id)
        commit.statuses.create({'state': 'success', 'ref': mr.source_branch, 'target_url': ''})
        commit.statuses.create({'state': 'success', 'ref': mr.source_branch, 'target_url': '', 'name': 'build'})
        commit.statuses.create({'state': 'success', 'ref': mr.source_branch, 'target_url': '', 'name': 'static_check'})

    @on_exception
    def set_mr_pipeline_failed(self, mr):
        merge_commit_id = mr.sha
        commit = self.project.commits.get(merge_commit_id)
        commit.statuses.create({'state': 'failed', 'ref': mr.source_branch, 'target_url': ''})
        commit.statuses.create({'state': 'failed', 'ref': mr.source_branch, 'target_url': '', 'name': 'build'})
        commit.statuses.create({'state': 'failed', 'ref': mr.source_branch, 'target_url': '', 'name': 'static_check'})

    @on_exception
    def set_mr_pipeline_running(self, mr):
        merge_commit_id = mr.sha
        commit = self.project.commits.get(merge_commit_id)
        commit.statuses.create({'state': 'running'})
        commit.statuses.create({'state': 'running', 'name': 'build'})
        commit.statuses.create({'state': 'running', 'name': 'static_check'})

    @on_exception
    def get_project_mr_approval_rule_list(self):
        return self.project.approvalrules.list()

    @on_exception
    def create_project_mr_common_approval(self):
        self.project.approvals.update(merge_requests_author_approval=False)
        self.project.approvalrules.create({
            'name': 'All Members',
            'rule_type': 'any_approver',
            'approvals_required': 2
        })

    @on_exception
    def create_project_mr_quality_approval(self):
        self.project.approvalrules.create({
            'name': 'nvos-code-quality-comitee',
            'rule_type': 'regular',
            'approvals_required': 1,
            'group_ids': [3677]
        })

    @on_exception
    def delete_project_mr_quality_approval(self):
        approval_list = self.project.approvalrules.list()
        for approval in approval_list:
            if approval.name == "nvos-code-quality-comitee":
                self.project.approvalrules.delete(approval.id)
            elif approval.name == "Release Gate":
                self.project.approvalrules.delete(approval.id)

    @on_exception
    def create_project_mr_protect_branch_approval(self, protect_branch_id):
        self.project.approvalrules.create({
            'name': 'Release Gate',
            'rule_type': 'regular',
            'approvals_required': 1,
            'user_ids': [1636, 1616, 1615, 573, 5, 486, 1296, 1283],
            'protected_branch_ids': [protect_branch_id]
        })

    @on_exception
    def edit_project_push_rules(self, message_rule):
        push_rule = self.project.pushrules.get()
        push_rule.commit_message_regex = message_rule
        push_rule.save()

    @on_exception
    def add_webhook(self, hook_url, token):
        hooks = self.project.hooks.list()
        for hook in hooks:
            if hook.url == hook_url:
                logging.info("The hook is exist, will not add")
                return
        logging.info("The hook is not exist, will add")
        # self.project.hooks.create({'url': hook_url, 'merge_requests_events': 1, 'token': token, 'push_events': 0,
        #                            'enable_ssl_verification': 0})
        self.project.hooks.create({'url': hook_url, 'merge_requests_events': 1, 'push_events': 0,
                                   'enable_ssl_verification': 0})

    @on_exception
    def delete_webhook(self, hook_url):
        hooks = self.project.hooks.list()
        for hook in hooks:
            if hook.url == hook_url:
                self.project.hooks.delete(hook.id)
                break

    # remove source branch when mr is merged
    @on_exception
    def create_merge_request_squash(self, source_branch, target_branch, mr_title):
        mr_list = self.project.mergerequests.list(source_branch=source_branch,
                                                  target_branch=target_branch, state='opened')
        if mr_list:
            logging.info("The mr list is exist %s" % (mr_list))
            return mr_list[0]
        mr = self.project.mergerequests.create({'source_branch': source_branch,
                                                'target_branch': target_branch,
                                                'title': mr_title,
                                                'remove_source_branch': True,
                                                'squash': True})
        return mr

    # branch_name must the protected branch list, can be: release*
    @on_exception
    def delete_protected_branch(self, branch_name):
        self.project.protectedbranches.delete(branch_name)

    @on_exception
    def get_protected_branch(self, branch_name):
        return self.project.protectedbranches.get(branch_name)

    @on_exception
    def create_protected_branch(self, branch_name):
        # default owner is: DigitalSystem Service Account digitalsystem-sa & Mauricio Inguanzo
        return self.project.protectedbranches.create({
            'name': branch_name,
            'allowed_to_push': [{'access_level': 40, "user_id": 609}, {'access_level': 40, "user_id": 5},
                                {'access_level': 0}],
            'allowed_to_merge': [{'access_level': 30}],
            'code_owner_approval_required': False,
            'allow_force_push': False
        })

    @on_exception
    def create_protected_branch_vdf(self, branch_name):
        # default owner is: DigitalSystem Service Account digitalsystem-sa & Mauricio Inguanzo
        return self.project.protectedbranches.create({
            'name': branch_name,
            'allowed_to_push': [{'access_level': 0}],
            'allowed_to_merge': [{'access_level': 30}],
            'code_owner_approval_required': False,
            'allow_force_push': False
        })

    @on_exception
    def freeze_branch(self, branch_name):
        self.project.protectedbranches.create({
            'name': branch_name,
            'allowed_to_push': [{'access_level': 0}],
            'allowed_to_merge': [{'access_level': 0}],
            'code_owner_approval_required': False,
            'allow_force_push': False
        })

    @on_exception
    def get_protected_tag_by_re(self, tag_name):
        protected_tag_list = self.project.protectedtags.list()
        for tag_obj in protected_tag_list:
            logging.info(tag_obj.name)
            if re.match(tag_obj.name, tag_name):
                logging.info("The pattern %s protected %s" % (tag_obj.name, tag_name))
                return tag_obj.name
        return None

    @on_exception
    def get_protected_branch_by_re(self, branch_name):
        protected_branch_list = self.project.protectedbranches.list()
        for branch_obj in protected_branch_list:
            logging.info(branch_obj.name)
            if re.match(branch_obj.name, branch_name):
                logging.info("The pattern %s protected %s" % (branch_obj.name, branch_name))
                return branch_obj.name
        return None

    @on_exception
    def get_protected_tag(self, tag_name):
        return self.project.protectedtags.get(tag_name)

    @on_exception
    def delete_protected_tag(self, tag_name):
        self.project.protectedtags.delete(tag_name)

    @on_exception
    def create_protected_tag(self, tag_name):
        self.project.protectedtags.create({
            'name': tag_name,
            'allowed_to_create': [{'access_level': 40, "user_id": 609}, {'access_level': 40, "user_id": 5},
                                  {'access_level': gitlab.const.MAINTAINER_ACCESS}],
        })

    """
        get_mr_list("merged/opened/all", "2022-03-01T08:00:00Z", "2022-04-01T08:00:00Z")
    """

    @on_exception
    def get_mr_list(self, target_branch, state, start_time, end_time):
        return self.project.mergerequests.list(state=state, target_branch=target_branch,
                                               updated_after=start_time, updated_before=end_time)

    @on_exception
    def get_repo_xml_project_revision(self, xml_file, branch_name, project_name):
        file_obj, content = self.get_file_obj_and_content(xml_file, branch_name)
        cur_xml_parse = XmlParse(filecontent=content)
        cur_xml_parse.get_project_node_list()
        for c_p_node in cur_xml_parse.get_project_node_list():
            if c_p_node.attrib['name'] == project_name:
                if 'revision' in c_p_node.attrib and c_p_node.attrib['revision']:
                    return c_p_node.attrib['revision'].replace("refs/tags/", "")
                break
        return None

    @on_exception
    def get_mr_diff(self, mr_iid):
        mr = self.project.mergerequests.get(mr_iid)
        result = mr.changes()
        changes = result["changes"]
        return changes


def create_merge_request(token, url, git_namespace_list_str, source_branch, target_branch, temp_branch, title):
    git_namespace_list = git_namespace_list_str.split(",")
    logging.info("The input namepsace list is %s" % (git_namespace_list))
    for git_namespace in git_namespace_list:
        git_obj = GitlabProject(git_namespace, token=token, gitlab_url=url)
        # must be target_branch compare with source_branch
        diff_commits = git_obj.compare_two_ref(target_branch, source_branch)
        if len(diff_commits['commits']) == 0:
            logging.info("The %s has no diff from %s to %s" % (git_namespace, source_branch, target_branch))
        else:
            logging.info("Delete branch %s " % (temp_branch))
            git_obj.delete_branch(temp_branch)
            logging.info("Create %s branch from %s" % (temp_branch, target_branch))
            git_obj.create_branch_retry(temp_branch, target_branch)
            logging.info("Create mr from %s to %s, with title %s" % (source_branch, temp_branch, title))
            mr = git_obj.create_merge_request(source_branch, temp_branch, title, False)
            logging.info("Auto merge %s" % (mr))
            git_obj.auto_merge(mr.iid)
            logging.info("Create mr from %s to %s, with title %s" % (temp_branch, target_branch, title))
            rlt_mr = git_obj.create_merge_request_squash(temp_branch, target_branch, title)
            logging.info("The result mr is %s, url is %s" % (rlt_mr, rlt_mr.web_url))
            # print msg for pipeline get the returnStdout
            print("The merge request url is : %s" % (rlt_mr.web_url))


def check_args(stage, *args):
    logging.info(args)
    for key in args:
        if not key:
            logging.error("The args is error, %s will do nothing" % (stage))
            sys.exit(1)


def arg_parse():
    parser = argparse.ArgumentParser(description='manual to this script')
    parser.add_argument('--type', type=str, default=None, help="Pls input the type: create_mr")

    # commont parms
    parser.add_argument('--url', type=str, default="https://githost.nevint.com", help="The gitlab url")
    parser.add_argument('--token', type=str, default="", help="The gitlab token")

    # create mr
    parser.add_argument('--git-namespace-list-str', type=str, default=None,
                        help="The git namespace needs to be create mr")
    parser.add_argument('--source-branch', type=str, default=None, help="The source branch")
    parser.add_argument('--target-branch', type=str, default=None, help="The target branch")
    parser.add_argument('--temp-branch', type=str, default=None, help="The temp branch")
    parser.add_argument('--title', type=str, default=None, help="The merge title")

    # add webhooks
    parser.add_argument('--project_namespace', type=str, default=None, help="The gitlab project namespace, such as ds/pipeline")
    parser.add_argument('--hook_url', type=str, default=None, help="The jenkins hook url")
    parser.add_argument('--git_event', type=str, default=None, help="The git trigger event")

    # handle mr
    parser.add_argument('--mr_id', type=str, default=None, help="The mr id")

    args = parser.parse_args()
    print(args)
    if args.type == 'create_mr':
        check_args(args.type, args.token, args.url, args.git_namespace_list_str, args.source_branch, args.target_branch,
                   args.temp_branch, args.title)
        create_merge_request(args.token, args.url, args.git_namespace_list_str, args.source_branch, args.target_branch,
                             args.temp_branch, args.title)
    if args.type == 'add_webhook':
        check_args(args.type, args.token, args.url, args.hook_url, args.git_event, args.project_namespace)
        project_obj = GitlabProject(args.project_namespace, args.token, args.url)
        project_obj.add_webhook(args.hook_url, args.token)
    elif args.type == 'rm_approval':
        check_args(args.type, args.token, args.url, args.project_namespace, args.mr_id)
        project_obj = GitlabProject(args.project_namespace, args.token, args.url)
        project_obj.remove_mr_approve(args.mr_id)
    else:
        logging.error("The type is un defined, use -h to see the help doc")

if __name__ == "__main__":
    arg_parse()
