#!/usr/bin/env python
# encoding: utf-8

import requests
import json
import logging
import sys
import time

class API:
    def __init__(self,gitlab_token,gitlab_url):
        self.gitlab_token= gitlab_token
        self.gitlab_baseUrl = gitlab_url+"/api/v4" # https://githost.nevint.com/api/v4
        logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', level=logging.INFO)

    def getAllMR(self, projectId, targetBranch):
        '''
        根据projectId和targetBranch获取所有mr
        :param projectId:
        :param targetBranch:
        :return:
        '''
        url = self.gitlab_baseUrl+"/projects/"+projectId+"/merge_requests"
        result = []
        payload = {'state': 'opened', 'target_branch': targetBranch}
        r = requests.get(url, headers={'PRIVATE-TOKEN': self.gitlab_token}, params=payload)
        if r.status_code==200:
            result = json.loads(r.text)
        return result

    def addMRComment(self, projectId, mrIid, comment):
        '''
        add comment to single mr
        :param projectId:
        :param mrIid:
        :param comment:
        :return:
        '''
        commentId = ""
        url = self.gitlab_baseUrl+"/projects/"+projectId+"/merge_requests/"+mrIid+"/notes"
        payload = {'body': comment}
        r = requests.post(url, headers={'PRIVATE-TOKEN': self.gitlab_token}, params=payload)
        if r.status_code==200:
            response = json.loads(r.text)
            commentId = response["id"]
        return commentId

    def getPipelineFromMR(self, projectId, mrIid):
        '''
        获取mr的comment获取Jenkins pipeline
        :param projectId:
        :param mrIid:
        :return:
        '''
        url = self.gitlab_baseUrl+"/projects/"+projectId+"/merge_requests/"+mrIid+"/notes?order_by=updated_at"
        buildUrl = ""
        r = requests.get(url, headers={'PRIVATE-TOKEN': self.gitlab_token})
        if r.status_code==200:
            comment_list = json.loads(r.text)
            for comment in comment_list:
                if comment["body"].startswith("Jenkins pipeline triggered by gitlab"):
                    buildUrl = "http" + comment["body"].split("http")[1].strip()
                    # 只取消最新触发的流水线
                    break
        return buildUrl

    def get_project_id(self, repoUrl):
        '''
        根据http形式的repoUrl查询projectId
        :param repoUrl:
        :return:
        '''
        repoUrl = repoUrl.strip()
        url = self.gitlab_baseUrl + "/projects"
        projectId = ""
        projectName = repoUrl.split(".git")[0].split("/")[-1]
        payload = {'simple': True, 'search': projectName}
        r = requests.get(url, headers={'PRIVATE-TOKEN': self.gitlab_token}, params=payload)
        if r.status_code == 200:
            projectList = json.loads(r.text)
            for project in projectList:
                if project["http_url_to_repo"] == repoUrl:
                    projectId = str(project["id"])
        return projectId

    def get_user_id_by_username(self, userName):
        '''
        根据域账号获取userId
        :param userName:
        :return:
        '''
        userName = userName.strip()
        url = self.gitlab_baseUrl + "/users"
        userId = -1
        payload = {'username': userName}
        r = requests.get(url, headers={'PRIVATE-TOKEN': self.gitlab_token}, params=payload)
        if r.status_code == 200:
            userList = json.loads(r.text)
            if len(userList) > 0:
                userId = userList[0]["id"]
        return userId

    def add_project_member(self, projectId, userId, access_level):
        '''
        为单个仓库增加member，并赋权
        :param projectId:
        :param userId:
        :param access_level:
        :return:
        '''
        url = self.gitlab_baseUrl + "/projects/" + projectId + "/members"
        payload = {'user_id': userId, 'access_level': access_level}
        r = requests.post(url, headers={'PRIVATE-TOKEN': self.gitlab_token}, params=payload)
        if r.status_code == 200 or r.status_code == 201:
            return True
        return False

    def get_group_id_by_group_name(self, group_name):
        '''
        根据group_name获取group_id
        :param group_name:
        :return:
        '''
        url = self.gitlab_baseUrl + '/groups'
        payload = {
            'search': group_name
        }
        resp = requests.get(url, headers={'PRIVATE-TOKEN': self.gitlab_token}, params=payload, verify=False)
        return resp.json()[0]['id']

    def get_project_list_by_group_id(self, group_id):
        '''
        根据group_id，获取该组下的所有项目
        :param group_id:
        :return:
        '''
        url = self.gitlab_baseUrl + '/groups/' + str(group_id) + '/projects'
        rs = []
        page_num = 1
        while True:
            payload = {
                'page': page_num,
                'per_page': 100
            }
            resp = requests.get(url, headers={'PRIVATE-TOKEN': self.gitlab_token}, params=payload, verify=False)
            page_num += 1
            if len(resp.json()) == 0:
                break
            rs += resp.json()

        return rs

    def modify_default_branch(self, project_id, new_default_branch):
        '''
        更改单个项目的默认分支
        :param project_id:
        :param new_default_branch:
        :return:
        '''
        url = self.gitlab_baseUrl + '/projects/' + str(project_id)
        requests.put(
            url,
            data={
                'default_branch': new_default_branch
            },
            headers={'PRIVATE-TOKEN': self.gitlab_token},
            verify=False
        )

    def create_approval_rule_review(self,projectId,name,approvals_required):
        '''
        为单个仓库创建approval rule
        如果要设置userId，curl方式要更稳定，Python请求无法处理[1,2,3]这种入参
        curl --request PUT --header "PRIVATE-TOKEN: sJ5tbHxtG2pmg_sz_YPw"  --header 'Content-Type: application/json' --data '{"name": "mandatory_approver", "approvals_required": 1, "user_ids": [1587, 1507, 169, 1768, 1758, 2272, 1740, 1975]}' https://githost.nevint.com/api/v4/projects/projectId/approval_rules/rule_id
        :param projectId:
        :return:
        '''
        url = self.gitlab_baseUrl + "/projects/" + str(projectId) + "/approval_rules"
        payload = {'approvals_required': approvals_required, 'name': name, 'applies_to_all_protected_branches':True}
        r = requests.post(url, headers={'PRIVATE-TOKEN': self.gitlab_token}, params=payload)
        if r.status_code == 200 or r.status_code == 201:
            return True
        return False

    def get_all_approval_rule(self,projectId):
        '''
        获取当前项目所有的approval_rule
        :param projectId:
        :return:
        '''
        url = self.gitlab_baseUrl + "/projects/" + str(projectId) + "/approval_rules"
        r = requests.get(url, headers={'PRIVATE-TOKEN': self.gitlab_token})
        if r.status_code == 200:
            rule_list = json.loads(r.text)
            return rule_list
        return []

    def delete_approval_rule(self,projectId,approval_rule_id):
        '''
        删除某个approval_rule
        :param projectId:
        :param approval_rule_id:
        :return:
        '''
        url = self.gitlab_baseUrl + "/projects/" + str(projectId) + "/approval_rules/" + str(approval_rule_id)
        r=requests.delete(
            url,
            headers={'PRIVATE-TOKEN': self.gitlab_token},
            verify=False
        )
        if r.status_code == 204:
            return True
        return False

    def list_hook(self,projectId):
        '''
        列出项目的webhook
        :param projectId:
        :return:
        '''
        url = self.gitlab_baseUrl + "/projects/" + str(projectId) +"/hooks"
        r = requests.get(url, headers={'PRIVATE-TOKEN': self.gitlab_token})
        if r.status_code == 200:
            list = json.loads(r.text)
            return list
        return []

    def add_mr_hook(self,projectId, webhook_url):
        '''
        为一个项目添加mr事件webhook
        :return:
        '''
        url = self.gitlab_baseUrl + "/projects/" + str(projectId) +"/hooks"
        payload = {'url': webhook_url, 'merge_requests_events': True, 'enable_ssl_verification':True}
        r = requests.post(url, headers={'PRIVATE-TOKEN': self.gitlab_token}, data=payload)
        if r.status_code == 200 or r.status_code == 201:
            logging.info("add mr hook success, projectId= %s " % projectId)
            return True
        logging.error("add mr hook failed, projectId= %s " % projectId)
        return False

# if __name__=='__main__':
#     # 设置日志输出等级
#     logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', level=logging.INFO)
#     api = API("cBQaud32m1ixN6i5aRBG", "https://githost.nevint.com")
#     lists = api.get_mr_diffs("10864","18")
#     print(lists)




