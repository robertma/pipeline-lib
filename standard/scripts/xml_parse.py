#!/usr/bin/python3

import xml.etree.cElementTree as ET
import os
import sys

# for the xml parse exception
def on_exception(actual_do):
    def wrap(*args, **keyargs):
        try:
            return actual_do(*args, **keyargs)
        except Exception as e:
            print("Error execute xml parse function: %s, error info: %s" % (actual_do.__name__, e))
            sys.exit(1)
    return wrap

class XmlParse:
    """
        pase xml file, and return project node list
    """
    @on_exception
    def __init__(self, **kwargs):
        if "filename" in kwargs:
            self.filename = kwargs["filename"]
            self.path = os.path.dirname(self.filename)
            self.tree = ET.ElementTree(file=self.filename)
            rlt = self.get_all_project_node_list()
            self.all_project_node_list = rlt[0]
            self.all_project_namespace_set = rlt[1]
        elif "filecontent" in kwargs:
            self.filecontent = kwargs["filecontent"]
            self.tree = ET.ElementTree(ET.fromstring(self.filecontent))
        else:
            print("Error input param for xmlparse")

    @on_exception
    def get_node_iter_by_name(self, name):
        return self.tree.iter(tag=name)

    @on_exception
    def get_remote_node_list(self):
        return [ele for ele in self.tree.iter(tag="remote")]

    @on_exception
    def get_default_node_list(self):
        return [ele for ele in self.tree.iter(tag="default")]

    @on_exception
    def get_include_node_list(self):
        return [ele for ele in self.tree.iter(tag="include")]

    @on_exception
    def get_project_node_list(self):
        return [ele for ele in self.tree.iter(tag="project")]

    """
        only get one include xml file, if the include file has another include file, will no work
        project node such as:
        {'name': 'ds/nvos-aurix-cdc-demo', 'path': 'ecus/tc377tx-eval', 'groups': '', 'project': '10054'}
    """
    @on_exception
    def get_all_project_node_list(self):
        project_node_list = self.get_project_node_list()
        inc_node_list = self.get_include_node_list()
        for inc_node in inc_node_list:
            inc_filename = inc_node.attrib['name']
            inc_filepath = os.path.join(self.path, inc_filename)
            inc_tree = ET.ElementTree(file=inc_filepath)
            for ele in inc_tree.iter(tag="project"):
                project_node_list.append(ele)
        project_namespace_set = set([node.attrib['name'] for node in project_node_list])
        return project_node_list, project_namespace_set

if __name__ == "__main__":
    pass
