#!/usr/bin/python

import argparse
import requests
import json
import sys
from retry import retry
# to disable warning info
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
import logging
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(filename)s[line:%(lineno)d] - %(levelname)s: %(message)s')


# for the api exception
def on_exception(actual_do):
    def wrap(*args, **keyargs):
        try:
            return actual_do(*args, **keyargs)
        except Exception as e:
            logging.error("Error execute rocket function: %s, error info: %s" % (actual_do.__name__, e))
            sys.exit(1)
    return wrap


class RocketApi:
    """
        api with rocket
    """
    def __init__(self, username="", password="", url="http://10.110.77.127:8000"):
        self.headers = {
            "Content-Type": "application/json",
        }
        self.body = {
            "username": username,
            "password": password
        }
        self.url = url
        self.token_url = self.url + "/api/token/"
        response = requests.request("POST", url=self.token_url, headers=self.headers, data=json.dumps(self.body))
        res_data = json.loads(response.text)
        logging.info(res_data)
        self.api_token = res_data['data']['access']
        self.access_headers = {
            "Authorization": "Bearer " + self.api_token,
            "Content-Type": "application/json"
        }
    
    @on_exception
    @retry(exceptions=Exception, tries=3, delay=5, backoff=3)
    def get(self, api, params=None):
        logging.info("api is %s" % api)
        response = requests.request("GET", url=self.url + "/api" + api, headers=self.access_headers, verify=False, params=params)
        res_data = json.loads(response.text)
        logging.info(res_data)
        return res_data

    @on_exception
    @retry(exceptions=Exception, tries=3, delay=5, backoff=3)
    def post(self, api, body):
        logging.info("api is %s, body is %s" % (api, body))
        response = requests.request("POST", url=self.url + "/api" + api, headers=self.access_headers, data=json.dumps(body))
        res_data = json.loads(response.text)
        logging.info(res_data)
        return res_data

    @on_exception
    def post_cicd_build_data(self, body):
        res_data = self.post('/cicd/build/', body)
        return res_data['data']


def check_args(stage, *args):
    logging.info(args)
    for key in args:
        if not key:
            logging.error("The args is error, %s will do nothing" % (stage))
            sys.exit(1)


def arg_parse():
    parser = argparse.ArgumentParser(description='manual to this script')
    parser.add_argument('--type', type=str, default=None, help="Pls input the type: cicd_post_build")

    # commont parms
    parser.add_argument('--url', type=str, default="http://10.110.77.127:8000", help="The web url")
    parser.add_argument('--username', type=str, default="", help="The web username")
    parser.add_argument('--password', type=str, default="", help="The web password")

    # cicd_post_build
    parser.add_argument('--build_data', type=str, default="{}", help="The build data dict as str")

    args = vars(parser.parse_args())
    if args["type"] == 'cicd_post_build':
        check_args(args["type"], args["url"], args["username"], args["password"], args["build_data"])
        rocket_obj = RocketApi(args["username"], args["password"], args["url"])
        logging.info(args)
        rocket_obj.post_cicd_build_data(eval(args["build_data"]))
    else:
        logging.error("The type is un defined, use -h to see the help doc")



if __name__ == "__main__":
    arg_parse()
