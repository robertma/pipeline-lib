#!/usr/bin/python3

import requests
import json

class FeishuApi:
    """
        Feishu Api
    """
    def __init__(self, app_id, app_secret):
        headers = {
            "Content-Type": "application/json",
        }
        body = json.dumps({
            "app_id": app_id,
            "app_secret": app_secret 
        })
        url = "https://open.feishu.cn/open-apis/auth/v3/tenant_access_token/internal"
        try:
            response = requests.request("POST", url, headers=headers, data=body)
            res_data = json.loads(response.text)
            api_token = res_data['tenant_access_token']
            self.access_headers = {
                "Authorization": "Bearer " + api_token,
                "Content-Type": "application/json; charset=utf-8"
            }
        except Exception as e:
            print("Get app token error: %s" % e)
    
    def send_message_to_chat_list(self, chat_id_list, content_dict):
        url = "https://open.feishu.cn/open-apis/im/v1/messages?receive_id_type=chat_id"
        for chat_id in chat_id_list:
            body = json.dumps({
                "receive_id": chat_id,
                "msg_type": "interactive",
                "content": json.dumps(content_dict)
            })
            try:
                response = requests.request("POST", url, headers=self.access_headers, data=body)
                res_data = json.loads(response.text)
                if res_data['msg'] == "ok":
                    print("Send message success")
                else:
                    print("Send message error %s" % res_data)
            except Exception as e:
                print("Send message error: %s" % e)

    def send_message_to_user_with_email(self, email, content_dict, message_type="interactive"):
        url = "https://open.feishu.cn/open-apis/im/v1/messages?receive_id_type=email"
        body = json.dumps({
            "receive_id": email,
            "msg_type": message_type,
            "content": json.dumps(content_dict)
        })
        try:
            response = requests.request("POST", url, headers=self.access_headers, data=body)
            res_data = json.loads(response.text)
            if res_data['msg'] == "ok":
                print("Send message success")
            else:
                print("Send message error %s" % res_data)
        except Exception as e:
            print("Send message error: %s" % e)

    def send_group_message(self, grou_url, body):
        headers = {
            'Content-Type': 'application/json'
        }
        try:
            response = requests.request("POST", grou_url, headers=headers, data=json.dumps(body))
            res_data = json.loads(response.text)
            if res_data['StatusCode'] == 0:
                print("Send message success")
            else:
                print("Send message error %s" % res_data)
        except Exception as e:
            print("Send message error: %s" % e)
    
    def get_robot_chart_id_list(self):
        url = "https://open.feishu.cn/open-apis/im/v1/chats"
        chat_id_list = []
        try:
            response = requests.request("GET", url, headers=self.access_headers)
            res_data = json.loads(response.text)
            if res_data['code'] == 0:
                print("Get chat id list success")
                chat_id_list = map(lambda x:x['chat_id'], filter(lambda x: x['name'], res_data['data']['items']))
            else:
                print("Send message error %s" % res_data)
        except Exception as e:
            print("Get chat id list error: %s" % e)
        return chat_id_list

class Send_Group_Message:
    def send_group_message(self, grou_url, body):
        headers = {
            'Content-Type': 'application/json'
        }
        try:
            response = requests.request("POST", grou_url, headers=headers, data=json.dumps(body))
            res_data = json.loads(response.text)
            if res_data['StatusCode'] == 0:
                print("Send message success")
            else:
                print("Send message error %s" % res_data)
        except Exception as e:
            print("Send message error: %s" % e)

if __name__ == '__main__':
    pass
