#!/usr/bin/python3
#coding:utf-8

# import os
import sys
import json
# import logging
from gitlab_api import GitlabProject, Gitlab

if __name__=='__main__':

    git_url = sys.argv[1]
    namespace = sys.argv[2]
    token = sys.argv[3]
    mr_iid = sys.argv[4]

    project_obj = GitlabProject(namespace, token, git_url)
    changes = project_obj.get_mr_diff(mr_iid)
    changed_ymls = ""
    dict = {}
    for item in changes:
        if (".yml" in item["new_path"] or ".yaml" in item["new_path"]) and ("jobs/" in item["new_path"]):
            file = item["new_path"]
            if item["deleted_file"]:
                dict[file] = "deleted"
            elif item["renamed_file"]:
                dict[file] = "renamed"
            else:
                dict[file] = "changed"
            # if item["deleted_file"] or item["renamed_file"]:
            #     # 忽略
            #     continue
            # else:
            #     changed_ymls = changed_ymls + item["new_path"] + ";"
    changed_ymls = json.dumps(dict)
    print(changed_ymls)