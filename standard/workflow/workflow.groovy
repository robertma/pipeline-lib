@Library('demo_lib') _

// 前置步骤，在容器外任意节点执行，无需安装任何软件无需任何配置
// node("dreamserver"){
//     stage("Before Start"){
//         try{
//             before_pipeline()
//         }catch(Exception e){
//             println("fail")
//             tools.send_feishu("failed")
//         }
//     }
// }

pipeline {
    agent any
    // agent {
    //     kubernetes {
    //         defaultContainer 'default'
    //         yaml """\n${k8s_yml_text}\n"""
    //     }
    // }
    options {
        gitLabConnection("gitlab")
        timeout(time:180, unit:'MINUTES')
        //skipDefaultCheckout()
        //parallelsAlwaysFailFast()
    }
    
    environment {
        CODE_FOLDER_NAME = "sourceCode"
        REPO_MIRROR_BASE_DIR = "/data/mirror_repo"
        PIPELINE_NAME = "MR CHECK"
        GITLAB_USERNAME = "robertma"
        GITLAB_US_TOKEN = "tmp"
        //GITLAB_CN_TOKEN = credentials('gitlab.com')
        GITLAB_US_URL = "https://gitlab.com"
        GITLAB_CN_URL = "https://gitlab.com"
        // ARTIFACTORY_USER_PASS = credentials('artifactory_dpd-jenkins-sa')
        DEFAULT_ARTIFACTORY_BASE_DIR = "https://artifactory-cn.nevint.com:443/artifactory/dpd-generic-local/standard/"
        STANDARD_CODE_PATH = "${env.WORKSPACE}/code"
        STANDARD_SCRIPT_PATH = "${env.WORKSPACE}/standard_scripts/"
    }

    stages {
        stage("Before Start"){
            steps {
                script {
                    echo "no before"
                    before_pipeline()
                }
            }
        }
        stage("Prepare") {
            steps {
                script {
                    // 前置操作，如下载config代码
                    setup()
                    // 执行默认pre-check以及用户自定义检查项，在拉代码之前执行
                    //pre_check()
                    // 执行正式stage
                    echo "env.SKIP_AND_SUCCESS = ${env.SKIP_AND_SUCCESS}"
                    if(env.SKIP_AND_SUCCESS == "false"){
                        stage("Code Prepare"){
                            code_prepare()
                        }
                        dir(env.CODE_FOLDER_NAME) {
                            // 根据yml动态生成其他stage
                            workflow_handler.do_stages(pipeline_obj)
                        }
                    }
                }
            }
        }
    }
    post {
        always {
            script {
                workflow_handler.send_build_history_to_web()
            }
        }
        aborted {
            script {
                workflow_handler.post_action("canceled")
            }
        }
        failure {
            script {
                //workflow_handler.post_action("failed")
                println("something wrong")
            }
        }
        success {
            script {
                workflow_handler.post_action("success")
            }
        }
    }
}

def setup(){
    // 下载配置config仓库代码
    checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: "${CONFIG_BRANCH}"]], extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: "$STANDARD_SCRIPT_PATH"],[$class: 'CleanBeforeCheckout'], [$class: 'WipeWorkspace']], userRemoteConfigs: [[credentialsId: 'robertma-gitlab.com', url: 'https://gitlab.com/robertma/pipeline.git']]]
    // 下载Standardized脚本文件
    def workflow_branch = scm.branches.first().getExpandedName(env.getEnvironment())
    checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: "${workflow_branch}"]], extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: "$STANDARD_CODE_PATH"],[$class: 'CleanBeforeCheckout'], [$class: 'WipeWorkspace']], userRemoteConfigs: [[credentialsId: 'robertma.gitlab-com', url: 'https://gitlab.com/robertma/pipeline-lib.git']]]

    // 初始化条件环境变量，后面步骤会更新的环境变量不能定义到environment{}中
    env.SKIP_AND_SUCCESS = "false"

    // 输出容器内所有环境变量，包含yml中解析出的环境变量
    echo "All environment variables: \n"
    sh "env"
}

def pre_check(){
    // 框架默认前置检查，如对某些mr进行跳过处理
    if (env.WORKFLOW_TEMPLATE == "mr") {
        mr_handler.before_mr_begin(pipeline_obj)
    }else if(env.WORKFLOW_TEMPLATE == "build"){
        workflow_handler.before_build_begin(pipeline_obj)
    }
    // 用户自定义前置检查
    workflow_handler.do_pre_custom_check(pipeline_obj)
}

def code_prepare(){
    // 根据pipeline模版分别做相应前置处理
    if (env.WORKFLOW_TEMPLATE == "mr") {
        // sync code
        code_handler.sync_source_code()
        // pre merge
        code_handler.merge_source_to_target(env.CODE_FOLDER_NAME, env.gitlabSourceBranch, env.gitlabTargetBranch)
    }else if(env.WORKFLOW_TEMPLATE == "build"){
        // sync code
        code_handler.sync_source_code()
        // repo pinned.xml保存
        code_handler.repo_pinned()
    }
}

def before_pipeline() {
    // 下载配置config仓库代码，加载job-yml及常用环境变量，设置全局变量pipeline_obj
    env.STANDARD_SCRIPT_PATH = "${env.WORKSPACE}/standard_scripts/"
    checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: "${CONFIG_BRANCH}"]], extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: "$STANDARD_SCRIPT_PATH"],[$class: 'CleanBeforeCheckout'], [$class: 'WipeWorkspace']], userRemoteConfigs: [[credentialsId: 'robertma-gitlab.com', url: 'https://gitlab.com/robertma/pipeline.git']]]
    def yml_file = "${STANDARD_SCRIPT_PATH}/${JOB_YML}"
    pipeline_obj=workflow_handler.parse_yml(yml_file)

    // 动态获取gitlabconnection
    gitlab_connection = tools.get_gitlabconnection()

    echo "gitlab_connection is ${gitlab_connection}"

    // 设置用户环境变量
    wrap([$class: 'BuildUser']) {
        script {
            env.BUILD_USER_ID = "${env.BUILD_USER_ID}"
            env.BUILD_USER = "${env.BUILD_USER}"
            env.BUILD_USER_EMAIL = "${env.BUILD_USER_EMAIL}"
        }
    }

    // 动态配置k8s yml
    echo "============== Do k8s configure ================"
    //k8s_yml_text = workflow_handler.config_k8s_pod(pipeline_obj)
    echo "============== k8s configure done, standardized pipeline is begainning ================"

}
