// workflow从哪里取
def GIT_PROJECT_ADDR = "https://gitlab.com/robertma/pipeline-lib.git"
def SCRIPT_PATH = "standard/workflow/workflow.groovy"
// 提取job文件夹
def groupName = pipeline['groupName']
// 基础信息
def folderName = groupName
def job_full_name = folderName+"/"+pipeline['jobName']
def DESCREPTION = pipeline['description']
// 用户输入的参数
def CUSTOM_PARAMETERS = pipeline['parameters']
// 模版相关
def WORKFLOW_TEMPLATE = pipeline['workflowTemplate']
def CRON_TRIGGER = pipeline['cronTrigger']
// 平台相关
def WEB_PIPELINE_ID = pipeline['webPipelineId']

// 创建不存在的folder
folder(folderName) {
    // displayName(folderName)
    description('created by standard dsl')
}

pipelineJob(job_full_name) {
    def job_description = ""
    if(DESCREPTION!=null && DESCREPTION.trim()!=""){
        job_description = job_description + DESCREPTION + "<br>"
    }
    job_description = job_description + "<a href=\"https://gitlab.com/robertma/pipeline/-/blob/${CONFIG_BRANCH}/${JOB_YML}\">Go pipeline yaml</a>"
    description(job_description)
    properties {
        buildDiscarder {
            strategy {
                logRotator {
                    daysToKeepStr("300")
                    numToKeepStr("3000")
                    artifactDaysToKeepStr("")
                    artifactNumToKeepStr("")
                }
            }
        }
        pipelineTriggers {
            triggers {
                if(WORKFLOW_TEMPLATE!=null){
                    if(WORKFLOW_TEMPLATE=="mr"){
                        gitlab {
                            branchFilterType('All')
                            triggerOnMergeRequest(true)
                            triggerOnPush(false)
                            triggerOpenMergeRequestOnPush("source")
                            triggerOnAcceptedMergeRequest(true)
                            triggerOnNoteRequest(true)
                            noteRegex("Jenkins please retry a build")
                        }
                    }
                }
                if(CRON_TRIGGER!=null){
                    cron {
                        spec(CRON_TRIGGER)
                    }
                }
            }
        }
    }
    parameters {
//        stringParam('JOB_YML', "$JOB_YML", '此标准化流水线对应的JOB_YML文件，自动生成，请勿修改！！！')
//        stringParam('CONFIG_BRANCH',"$CONFIG_BRANCH", '此标准化流水线对应的JOB_YML文件所在config仓库分支，自动生成，请勿修改！！！')
        booleanParam('DEBUG_MODE', false, '调试模式，如果勾选，则build失败可进入容器排查')
//        stringParam('WEB_PIPELINE_ID',"$WEB_PIPELINE_ID", '此标准化流水线对应的Web的主键Id，自动生成，请勿修改！！！')
        if(CUSTOM_PARAMETERS!=null){
            for(item in CUSTOM_PARAMETERS){
                stringParam(item["name"], item["defaultValue"], item["description"])
            }
        }
    }
    environmentVariables {
        env("JOB_YML", "$JOB_YML")
        env("CONFIG_BRANCH", "$CONFIG_BRANCH")
        env("WEB_PIPELINE_ID","$WEB_PIPELINE_ID")
    }

    definition {
        cpsScm {
            lightweight(true)
            scm {
                git{
                    remote{
                        url(GIT_PROJECT_ADDR)
                        credentials("robertma-gitlab.com")
                    }
                    branch(BRANCH_NAME)
                    extensions {
                        cleanBeforeCheckout()
                        wipeWorkspace()
                    }
                }
                scriptPath("$SCRIPT_PATH")
            }
        }
    }
}
